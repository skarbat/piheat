#
# Makefile
#
# Automate some tedious tasks
#

.PHONY: help tests clean

all: help



help:
	@echo "Makefile for the Piheat software package"
	@echo "Available rules:"
	@echo " tests, pkg, clean"

tests:
	pytest -v tests

pkg:
	debuild -us -uc -b
	dpkg -c ../piheat*deb

clean:
	-find . -iname "*~" -exec rm -rf {} \;
	-find . -iname "*pyc" -exec rm -rf {} \;
	-find . -iname "__pycache__" -exec rm -rf {} \;
	-rm tests/*rrd
	-rm tests/*png
	-rm -rf debian/.debhelper/ debian/piheat/
	-rm debian/files debian/piheat.debhelper.log debian/piheat.substvars
