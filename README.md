# PiHeat - RaspberryPI Temperature logger

A software package to monitor and graph temperature and humidity readings overtime, using the DHT-22 sensor.

## Overview

PiHeat provides a glue between the DHT-22 sensor and graphics from the RRD tool,
to monitor the temperature and humidity readings. It will also register the RaspberryPI temperature.

## Hardware setup

Follow the wiring instructions from either tutorial below:

 * https://www.instructables.com/Raspberry-Pi-Tutorial-How-to-Use-the-DHT-22/
 * https://tutorials-raspberrypi.com/raspberry-pi-measure-humidity-temperature-dht11-dht22/

Make sure to use GPIO4 for the data pin.

## Software Requirements

Grab a Raspbian Lite image from the official download site and boot it on the RaspberryPI:

 * https://www.raspberrypi.org/software/operating-systems/

Since the board will run headless, you might want to learn about how to network it if using a wireless connection:

 * https://raspberrytips.com/raspberry-pi-wifi-setup/

Once you have an ssh connection to the Raspberry, you will need to install the
Adafruit official DHT-22 driver on it:

```
$ sudo apt-get install git build-essential python-dev python-setuptools
$ git clone https://github.com/adafruit/Adafruit_Python_DHT.git
$ cd Adafruit_Python_DHT/
$ sudo python setup.py install
```

## Build & Install PiHeat

Type `make pkg`. Copy and install the resulting debian package on the RaspberryPI:

```
pi@raspberrypi:~ $ sudo apt-get install ./piheat_1.0-1_all.deb
```

Make sure that the sensor is working correctly:

```
pi@raspberrypi:~ $ sudo piheat-cli read
reading a sample
temperature=17.40 humidity=51.30 pitemp=35.4
```

The PiHeat will start collecting samples and generating graphics right away.
Hit the RaspberryPI from a browser at `http://raspberrypi.local`.

Samples will be collected every minute and graphics automatically refreshed on the page. You should see a page similar to this:

![Sample Graphs](piheat-web-sample.png)

## Alerts

The alerts module allows for detecting temperatures crossing a given threshold, then react proactively.
Upon reaching a given threshold, a message can be sent to a Slack channel for alert.

For details on setting up the Slack channel URL, read:

 * https://api.slack.com/tutorials/slack-apps-hello-world

Then, set your Slack URL into the `piheat-cli` variable `slack_hook_url`,
and maximum allowed temperature into `temperature_threshold` as a float number.
