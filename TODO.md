# Things TO-DO

Below is a list of things that would make this software better:

 * Extract settings from piheat-cli into a config file /etc/piheat.conf
 * Migrate to Python3 - is the Adafruit driver Python3 ready?
 * Integrate the Adafruit driver into the package
 * Provide SNMP traps for temperature peak alerts
