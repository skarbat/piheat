#
# alerts.py
#

import rrdtool
import urllib2
import json


def slack(hook_url, message):
    post = json.dumps ({ 'text': message + '\n' })
    request = urllib2.Request(url = hook_url,
                          data = post.encode('ascii'),
                          headers = {'Content-Type': 'application/json'})

    print 'contacting: {}...'.format(hook_url)
    response = urllib2.urlopen(request)


def is_peak_temperature(rrd_filename, max_temperature=35.0):

    last_sample = rrdtool.lastupdate(rrd_filename)
    last_temperature = last_sample['ds']['temperature']

    is_peak = last_temperature > max_temperature
    print 'last temperature={}, max_temp={} is_peak={}'.format(
        last_temperature, max_temperature, is_peak)

    return is_peak
