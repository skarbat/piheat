#
# graph.py
#

import rrdtool

def render(rrd_filename, png_filename, title, elapsed_secs=3600):
    rrdtool.graph(png_filename,
                  '--imgformat', 'PNG',
                  '--width', '540',
                  '--height', '200',
                  '--start', "-{}".format(elapsed_secs),
                  '--slope-mode',
                  '--end', 'now',
                  '--title', title,
                  '--vertical-label', 'Temperature C / Humidity %',
                  '--lower-limit', '0',
                  '--alt-autoscale-max',
                  'DEF:temperature={}:temperature:MAX'.format(rrd_filename),
                  'DEF:humidity={}:humidity:MAX'.format(rrd_filename),
                  'DEF:pitemp={}:pitemp:MAX'.format(rrd_filename),
                  'LINE1:temperature#ff0000:temperature',
                  'LINE1:humidity#0000ff:humidity',
                  'LINE1:pitemp#00ff00:pitemp')
