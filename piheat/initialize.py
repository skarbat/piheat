#
# initialize.py
#

import os
import rrdtool
import errno


def create_rrd(rrd_filename, overwrite=False):

    if not overwrite and os.path.isfile(rrd_filename):
        raise OSError(errno.EEXIST, 'RRD File already exists: {}'.format(rrd_filename))

    base_directory = os.path.dirname(rrd_filename)
    if len(base_directory) and not os.path.isdir(base_directory):
        print 'creating directory: {}'.format(base_directory)
        os.makedirs(base_directory)

    data_sources=[ 'DS:temperature:GAUGE:180:0:80',
                   'DS:humidity:GAUGE:180:0:100',
                   'DS:pitemp:GAUGE:180:0:100'
    ]

    print 'Initializing RRD database: {}'.format(rrd_filename)
    rrdtool.create( rrd_filename,
                    '--step', '60',
                    '--start', 'now - 7d',
                    data_sources,
                    'RRA:MAX:0.5:1:10080')


def info_rrd(rrd_filename):
    return rrdtool.info(rrd_filename)
