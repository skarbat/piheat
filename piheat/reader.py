#
# reader.py
#

import rrdtool
import os


def rpi_temperature():
    data = os.popen('vcgencmd measure_temp').read()
    t = data.split("=")[1].split("'")[0]
    return float(t)

def sensor(dht_pin=4, simulate=False):
    if simulate:
        import random
        temperature = '{}'.format(random.randint(0, 80))
        humidity = '{}'.format(random.randint(0, 100))
        pitemp = '{}'.format(random.randint(0, 100))
    else:
        import Adafruit_DHT as dht
        h, t = dht.read_retry(dht.DHT22, dht_pin)

        temperature = '%.2f' % t
        humidity = '%.2f' % h
        pitemp = rpi_temperature()

    return temperature, humidity, pitemp


def dump(rrd_filename, temperature, humidity, pitemp, timestamp=None):
    time_mark = 'N' if not timestamp else timestamp
    rrdtool.update(rrd_filename, '{}:{}:{}:{}'.format(time_mark, temperature, humidity, pitemp))
