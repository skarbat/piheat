#
# Test that the slack alert module is working
#

import threading
import time
import os

import piheat

# Set to true if tcp listener receives message from the test
succeed = False

def listen_for_alert():
    global succeed
    
    # Listen for tcp connection, send OK to unblock client
    data = os.popen('echo "OK" | nc -l -p 12345').read()
    if data.find('I am a test') != -1:
        succeed = True

def test_alert():
    slack_hook_url = 'http://localhost:12345'

    # start a thread, listens on tcp socket
    thread = threading.Thread(target = listen_for_alert)
    thread.daemon = True
    thread.start()
    time.sleep(1)

    # send a message to the thread
    response = piheat.alert.slack(slack_hook_url, "I am a test")
    time.sleep(1)

    # expect the thread to notify us of receipt
    assert succeed == True
