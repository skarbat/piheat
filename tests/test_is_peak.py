#
# test_threshold.py
#
# test that a temperature threshold hit is detected.
#

import piheat
import time

def test_is_peak():
    rrd_filename = 'tests/test_is_peak.rrd'

    piheat.initialize.create_rrd(rrd_filename, overwrite=True)

    now = time.time()

    piheat.reader.dump(rrd_filename, '35.5', '50.0', now - 240)
    piheat.reader.dump(rrd_filename, '40.5', '50.0', now - 180)
    piheat.reader.dump(rrd_filename, '41.2', '50.0', now - 120)
    piheat.reader.dump(rrd_filename, '39.8', '50.0', now - 60)
    piheat.reader.dump(rrd_filename, '44.2', '50.0', now)

    assert piheat.alert.is_peak_temperature(rrd_filename, max_temperature=42.0) == True
    assert piheat.alert.is_peak_temperature(rrd_filename, max_temperature=44.1) == True

    assert piheat.alert.is_peak_temperature(rrd_filename, max_temperature=44.2) == False
    assert piheat.alert.is_peak_temperature(rrd_filename, max_temperature=44.3) == False
