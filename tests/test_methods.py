
def test_call_methods():
    '''
    makes sure that the expected methods are implemented
    '''
    import piheat

    piheat.initialize.create_rrd
    piheat.reader.sensor
    piheat.reader.dump
    piheat.graph.render
    piheat.alert.slack
    piheat.alert.is_peak_temperature
