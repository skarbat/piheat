'''
Tests for loading the piheat module
'''

def test_import():
    import piheat

def test_import_initialize():
    from piheat import initialize

def test_import_reader():
    from piheat import reader

def test_import_graph():
    from piheat import graph
