
import time
import piheat

def test_simulate():
    '''
    Simulate a 1 hour of temperature and humidity sample readings
    Store on rrd tool, then generate a graphic
    '''    
    rrd_filename = 'tests/test_simulate.rrd'
    png_filename = 'tests/test_simulate.png'
    title = '1 hour simulated samples'

    piheat.initialize.create_rrd(rrd_filename, overwrite=True)

    time_behind = 60 * 60
    last_sample_time = time.time() - time_behind

    while time.time() > last_sample_time:
        t, h, p = piheat.reader.sensor(simulate=True)
        piheat.reader.dump(rrd_filename, t, h, p, timestamp=last_sample_time)
        last_sample_time += 60

    piheat.graph.render(rrd_filename, png_filename, title)
